/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author almiscovich
 */

  package com.example.myapp;
  import java.text.DecimalFormat;
public class HotChocolate implements Drink{
    DecimalFormat df2 = new DecimalFormat("###.##");
    double cost = 2.85;
    String name = "Hot Chocolate";

    public double getCost() {
        return  Double.valueOf(df2.format(cost));
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public double cost() {
        return  Double.valueOf(df2.format(cost));
    }

    @Override
    public String type() {
        return "Hot Chocolate \n";
    }
    
}
