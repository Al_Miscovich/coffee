/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author almiscovich
 */
 package com.example.myapp;

public interface Drink {
    public double cost();
    public String type();
}
