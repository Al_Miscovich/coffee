/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example.myapp;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author almiscovich
 */
public class Main {
    public static void main(String[] args){
        CustomerGUI customer = new CustomerGUI();
        customer.setVisible(true);

        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();

        exec.scheduleAtFixedRate(new Runnable() {
            final FelineFightersCoffeeGUI worker = new FelineFightersCoffeeGUI();

            @Override
            public void run() {
                worker.setVisible(true);
                worker.run();
            }
        }, 0, 100, TimeUnit.SECONDS);
    }
    }

